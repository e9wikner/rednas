import pytest

import rednas.filedb


def test_insert_raises_ItemAlreadyInserted(
    filedb, local_identifier, local_suffix, local_timestamp
):
    with pytest.raises(rednas.filedb.ItemAlreadyInserted):
        filedb.insert(
            identifier=local_identifier,
            suffix=local_suffix,
            timestamp=local_timestamp,
        )


# TODO: test_sync_remote_file_raises_RemoteNotInSync

# TODO: test_lookup_remote_raises_RemoteNotSynced
