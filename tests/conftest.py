from pathlib import Path
import sys

srcdir = Path(__file__).parent.parent
sys.path.insert(0, str(srcdir.resolve()))

import pytest  # noqa E402

from rednas.filedb import FileDB  # noqa E402
import rednas.utils  # noqa E402


BASEDIR = Path(__file__).parent
FIXTURES_ROOT = BASEDIR / "fixtures"
SUFFIX = ".jpg"


LOCAL_FILES = [
    ("04DBA215-A77B-4175-A7F6-D243AC66E0FA-L0-001", 1585893880.0, 1587568261.6, 0, 0),
    ("84BC3DBC-8B3C-44EF-A8A3-B7C20F9A3B75-L0-001", 1588404265.7, 1588745372.9, 0, 0),
    ("874E88A1-91A6-41D0-A294-C85E4E6B487C-L0-001", 1588404262.1, 1588744915.3, 0, 0),
    ("D32C7C94-709C-42A1-A2A5-EAAF7B013191-L0-001", 1585893870.0, 1588744694.1, 0, 0),
]


REMOTE_FILES = [
    # unchanged
    LOCAL_FILES[0],
    #  removed after sync:
    ("84BC3DBC-8B3C-44EF-A8A3-B7C20F9A3B75-L0-001", 1588404265.7, 1588745372.9, 1, 0),
    # added after sync:
    ("B3889225-7D55-41F1-949A-97FFEAC8D304-L0-001", 1585318903.0, 1585897648.0, 0, 0),
    # updated after sync:
    ("D32C7C94-709C-42A1-A2A5-EAAF7B013191-L0-001", 1585893870.0, 1588750000.1, 0, 0),
    # removed before sync:
    ("CD068371-2650-4A33-BD22-CCF63A6D88A3-L0-001", 1588404260.4, 1588744881.8, 1, 0),
]


@pytest.fixture
def identifiers_to_be_added():
    return {LOCAL_FILES[0][0]}


@pytest.fixture
def identifiers_to_be_updated():
    return {
        "B3889225-7D55-41F1-949A-97FFEAC8D304-L0-001",
        "D32C7C94-709C-42A1-A2A5-EAAF7B013191-L0-001",
    }


@pytest.fixture
def identifiers_to_be_removed():
    return {
        "84BC3DBC-8B3C-44EF-A8A3-B7C20F9A3B75-L0-001",
    }


@pytest.fixture
def local_identifier():
    return LOCAL_FILES[0][0]


@pytest.fixture
def local_source_filepath(tmp_path):
    item_path = tmp_path / "file.jpg"
    item_path.touch()
    return item_path


@pytest.fixture
def local_target_filepath(local_timestamp, local_suffix):
    return rednas.utils.timestamp_to_filename(local_timestamp).with_suffix(local_suffix)


@pytest.fixture
def local_timestamp():
    return LOCAL_FILES[0][1]


@pytest.fixture
def local_suffix():
    return SUFFIX


@pytest.fixture
def remote_identifier():
    return REMOTE_FILES[2][0]


@pytest.fixture
def remote_timestamp():
    return REMOTE_FILES[2][1]


@pytest.fixture
def remote_filepath():
    return FIXTURES_ROOT / "IMG_20180107_151831.jpg"


@pytest.fixture
def clean_filedb(tmp_path):
    db = FileDB(root=tmp_path, device_id="local")
    db.create_files_table()
    return db


@pytest.fixture
def filedb(tmp_path):
    db = FileDB(root=tmp_path, device_id="tests_conftest_filedb_local")
    db.create_files_table()
    for identifier, timestamp, timestamp_modified, removed, size in LOCAL_FILES:
        db.insert(
            identifier=identifier,
            suffix=SUFFIX,
            timestamp=timestamp,
            modified_timestamp=timestamp_modified,
        )
    return db


@pytest.fixture
def filedb_on_remote(tmp_path, monkeypatch):

    old_safe_path = FileDB.convert_to_path_that_can_be_inserted

    def new_safepath_with_prefix(*args, **kwargs):
        return (
            f"file:///var/mobile/Media/DCIM/100APPLE/"
            f"{old_safe_path(*args, **kwargs)}"
        )

    monkeypatch.setattr(
        FileDB, "convert_to_path_that_can_be_inserted", new_safepath_with_prefix
    )

    db = FileDB(root=tmp_path, device_id="tests_conftest_filedb_remote")
    db.create_files_table()
    for identifier, timestamp, timestamp_modified, removed, size in REMOTE_FILES:
        db.insert(
            identifier=identifier,
            suffix=".jpg",
            timestamp=timestamp,
            modified_timestamp=timestamp_modified,
        )
        if removed:
            db.mark_as_removed(identifier=identifier)

    monkeypatch.undo()
    return db


@pytest.fixture
def filedb_with_synced_remote(filedb, filedb_on_remote):
    filedb.sync_remote_index(filedb_on_remote.db_path.open(mode="rb"))
    return filedb


@pytest.fixture
def mockup_usb_drive_inserted(monkeypatch):
    # the file is generated on ubuntu:~$ lsblk --json --tree --output-all
    mockup_file = FIXTURES_ROOT / "lsblk_output.json"
    lsblk_json = mockup_file.read_text()

    def mocked_run(*args, **kwargs):
        return lsblk_json.splitlines()

    monkeypatch.setattr(rednas.utils, "run", mocked_run)
