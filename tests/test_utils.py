from rednas import utils


def test_append_two_digit_counter(tmp_path):
    testfile = tmp_path / "temp.local_v0_db"
    testfile.touch()
    for i in range(10):
        expected_filename = f"temp_{i+1:02}.local_v0_db"
        new_path = utils.path_with_filename_collision_counter(testfile)
        filename = new_path.name
        assert expected_filename == filename
        new_path.touch()
