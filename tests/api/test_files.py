from concurrent.futures import ThreadPoolExecutor, wait

import pytest


def put_file(client, url, filepath):
    return client.simulate_put(
        url,
        body=filepath.read_bytes(),
        headers={"content-type": "application/octet-stream"},
    )


def test_put_file(client_in_sync):
    assert client_in_sync


def test_get_file(
    client_in_sync, api_url, device_id, remote_identifier, remote_filepath
):
    url = f"{api_url}/files/{device_id}/{remote_identifier}"
    response = client_in_sync.simulate_get(url)

    assert 200 == response.status_code, response.json["description"]
    assert remote_filepath.read_bytes() == response.content


def test_delete_file(client_in_sync, api_url, device_id, remote_identifier):
    url = f"{api_url}/files/{device_id}/{remote_identifier}"
    response = client_in_sync.simulate_delete(url)
    assert 200 == response.status_code
    response_get = client_in_sync.simulate_get(url)
    assert 404 == response_get.status_code


def test_update_file(
    client_in_sync,
    filedb_on_remote,
    api_url,
    device_id,
    remote_identifier,
    remote_filepath,
    remote_timestamp,
):
    with filedb_on_remote.transaction:
        filedb_on_remote._execute(
            "UPDATE local_files SET timestamp_modified=? WHERE identifier=?",
            parameters=(remote_timestamp + 5, remote_identifier),
        )
    url = f"{api_url}/index/{device_id}"
    response = client_in_sync.simulate_put(
        url,
        body=filedb_on_remote.db_path.read_bytes(),
        headers={"Content-Type": "application/octet-stream"},
    )
    assert 200 == response.status_code
    assert remote_identifier in response.json["missing"]

    url = f"{api_url}/files/{device_id}/{remote_identifier}"
    response = client_in_sync.simulate_put(
        url,
        body=remote_filepath.read_bytes(),
        headers={"content-type": "application/octet-stream"},
    )
    assert 200 == response.status_code


@pytest.mark.xfail
def test_insert_multiple_images_simultaneously(
    client_clean, api_url, device_id, remote_identifier, remote_filepath
):
    url = f"{api_url}/files/{device_id}/{remote_identifier}"
    with ThreadPoolExecutor() as executor:
        futures = {
            executor.submit(put_file, client_clean, url, remote_filepath)
            for _ in range(3)
        }

    complete, not_complete = wait(futures, timeout=1)
    assert not not_complete

    exceptions = [f.exception() for f in complete]
    assert not any(exceptions)

    responses = [f.result() for f in complete]
    status_codes = {r.status_code for r in responses}
    expected_status_codes = {200, 403}
    assert expected_status_codes == status_codes


# TODO tests:
#     insert file that already exists on disk
#     delete item from local_v0_db that doesn't exist in local_v0_db
