import sys

import pytest

import rednas.blockdevice

if not sys.platform == "linux":
    pytest.skip("skipping linux-only tests", allow_module_level=True)


def test_storage(client_clean, api_url, mockup_usb_drive_inserted):
    response = client_clean.simulate_get(f"{api_url}/storage/devices")
    blockdevices = response.json["blockdevices"]
    assert len(blockdevices) == 2

    external_drive = None
    for bd in blockdevices:
        assert set(bd.keys()) == {"name", "model", "vendor", "size", "children", "uuid"}
        if bd["name"] == "sda":
            external_drive = bd

    children = external_drive["children"]
    for child in children:
        assert set(child.keys()) == {
            "mountpoint",
            "name",
            "filesystem_type",
            "filesystem_size",
            "filesystem_available",
            "uuid",
            "relative_uri",
        }


def test_Partition_mount(client_clean, api_url, mockup_usb_drive_inserted, monkeypatch):
    def fake_mount(*args):
        return

    uuid = "cf4fbe0d-f132-4e56-a941-89b3f2f87867"
    monkeypatch.setattr(rednas.blockdevice.Partition, "mount", fake_mount)
    response = client_clean.simulate_get(f"{api_url}/storage/partition/{uuid}/mount")
    assert response.status_code == 200
