import sys

import pytest

import rednas.utils
import rednas.blockdevice

if not sys.platform == "linux":
    pytest.skip("skipping linux-only tests", allow_module_level=True)


@pytest.fixture
def disks(mockup_usb_drive_inserted):
    return rednas.blockdevice.disks()


def test_disk_blockdevices(disks):
    assert len(disks) == 2


@pytest.fixture
def disk(disks):
    return disks[0]


def test_BlockDevice_init(disk):
    assert disk.name == "sda"
    assert len(disk.children) == 1


def test_BlockDevice_as_dict(disk):
    disk_dict = disk.as_dict()
    assert disk_dict["size"] == "29.1G"


@pytest.fixture
def partition(disk):
    return disk.children[0]


def test_Partition_init(disk, partition):
    assert partition.name == "sda1"
    assert partition.parent is disk


def test_exception_UUIDNotFound(partition):
    with pytest.raises(rednas.blockdevice.UUIDNotFound) as exc:
        rednas.blockdevice.Partition.from_uuid("arne")
    assert "arne" in str(exc)
