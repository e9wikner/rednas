import json
from pathlib import Path

import pytest

from rednas import btrfs

BASEDIR = Path(__file__).parent


@pytest.fixture
def mockup_btrfs_filesystems(monkeypatch):
    # the file is generated on ubuntu:~$ sudo btrfs filesystem show --raw
    filesystem_show_file = BASEDIR / "fixtures" / "btrfs_filesystem_show_output.txt"
    btrfs_filesystem_show_output = filesystem_show_file.read_text()

    def mocked_show(*args, **kwargs):
        return btrfs_filesystem_show_output

    findmnt_file = BASEDIR / "fixtures" / "findmnt_output.json"
    findmnt_json = json.load(findmnt_file.open())

    def mocked_findmnt(*args, **kwargs):
        return findmnt_json["filesystems"]

    monkeypatch.setattr(btrfs.Btrfs, "show", mocked_show)
    monkeypatch.setattr(btrfs.Btrfs, "findmnt_filesystems", mocked_findmnt)


@pytest.fixture
def filesystems(mockup_btrfs_filesystems):
    return btrfs.Btrfs.yield_all()


def test_FileSystem_list_all(filesystems):
    assert len(list(filesystems)) == 3


@pytest.fixture
def filesystem(monkeypatch, mockup_btrfs_filesystems):

    mockup_text = """
Label: 'label_disk_raid0'  uuid: 8b46c91e-1aa7-4fdc-a513-c44cf0ecf89e
	Total devices 2 FS bytes used 2004221120512
	devid    1 size 4000785960960 used 1005030735872 path /dev/sdc1
	devid    2 size 4000785960960 used 1005030735872 path /dev/sdd1
"""

    def mocked_show(*args, **kwargs):
        return mockup_text

    monkeypatch.setattr(btrfs.Btrfs, "show", mocked_show)

    return btrfs.Btrfs("8b46c91e-1aa7-4fdc-a513-c44cf0ecf89e")


def test_FileSystem_label(filesystem):
    expected_label = "label_disk_raid0"
    assert filesystem.label == expected_label


def test_FileSystem_total_devices(filesystem):
    assert filesystem.total_devices == 2


def test_FileSystem_bytes_used(filesystem):
    assert isinstance(filesystem.bytes_used, int)


def test_FileSystem_devices(filesystem):
    assert filesystem.devices[1].path == "/dev/sdc1"


def test_FileSystem_mountpoint(filesystem):
    assert filesystem.mountpoint == "/mnt/btrfs_raid1/root"
