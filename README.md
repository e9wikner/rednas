# rednas

## Development

The code is formatted with *black* and linted with *pylint*.

Format all code with black:

    black  --target-version py38 --exclude 'vendor/*.?' rednas tests


Run dev-server:

    chmod +x run_dev_server
    ./run_dev_server


## Raspberry Pi image handling

NOTE! This is old info and needs update since there is no bundeled image.

The bundeled image is a clean install with these instructions: 
[Arch Linux Arm](https://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2)
When the image is installed and booted on the Pi v2 ssh into it and:
 - perform a full system upgrade: `pacman -Syu`
 - reboot and check it's ok
 - mount sd-card on another system
 - get some info of the card
 - shrink to minimum size (with gparted)
 - clone it to a file
 - write it back to a card
 - expand to maximum size (with gparted)
 - boot the Pi and check that it's ok
 
 ### get sd-card info:

    $ sudo lsblk -l  # to find /dev/sdx and check 
    Disk /dev/sdb: 29,29 GiB, 31439454208 bytes, 61405184 sectors
    Disk model: USB3.0 CRW   -SD
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes
    I/O size (minimum/optimal): 512 bytes / 512 bytes
    Disklabel type: dos
    Disk identifier: 0xbba05c24

    Device     Boot  Start     End Sectors  Size Id Type
    /dev/sdb1         2048  206847  204800  100M  c W95 FAT32 (LBA)
    /dev/sdb2       206848 5996543 5789696  2,8G 83 Linux
 
### clone it:

    sudo dd if=/dev/sdb bs=512 count=5996544 | armv7_rpi2.img.gz

### write back to sd-card:

    gzip armv7_rpi2.img.gz --decompress --to-stdout | sudo dd of=/dev/sdb bs=512 count=5996544 conv=fsync

### resizing:

Doing it manually for now but here are some hints on how to do it automatically:
https://raspberrypi.stackexchange.com/questions/79759/how-to-create-a-bootable-image-for-raspberry-pi
