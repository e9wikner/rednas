#!/usr/bin/env python3
"""This script is used to keep a remote server in sync with this directory.

Launch the script to build the Elm frontend and copy the code to the remote.
Setup the remote with your user and a ssh key to avoid entering passwords.
"""
from pathlib import Path
import argparse
import os
import subprocess
import time


PI = "rednas.local"
BASEDIR = Path(__name__).parent.parent.absolute()
REMOTE_DIR = "/srv/http/rednas"
INIT_COMMANDS = [
    f"mkdir -p {REMOTE_DIR} -m 770",
    f"groupadd www-data -f",
    f"usermod $USER -a -G www-data",
    f"chgrp www-data {REMOTE_DIR}",
]
EXCLUDES = [".*", "__pycache__", "elm-stuff", "*~"]


def run(commands, check=True):
    print(" ".join(commands))
    subprocess.run(commands, check=check)


def commands_to_bash_string(commands):
    return f'"{" && ".join(commands)}"'


def install(remote=PI):
    print(f"Installing packages on: {remote}")
    init_commands = commands_to_bash_string(INIT_COMMANDS)
    ssh_commands = ["ssh", remote, "sudo", "-S", "bash", "-c"]

    run(ssh_commands + [init_commands], check=True)
    rsync(remote=remote)

    install_commands = commands_to_bash_string(
        [f"cd {REMOTE_DIR}", "python3 install", "python3 configure"]
    )
    run(ssh_commands + [install_commands], check=True)


def config(remote=PI):
    print(f"Configuring {remote}")
    ssh_commands = ["ssh", remote, "sudo", "-S", "bash", "-c"]
    commands = commands_to_bash_string([f"cd {REMOTE_DIR}", "python3 configure"])

    rsync(remote=remote)
    run(ssh_commands + [commands], check=True)


def rsync(remote=PI):
    def yield_excludes():
        for e in EXCLUDES:
            yield "--exclude"
            yield e

    command = ("rsync", ".", f"{remote}:{REMOTE_DIR}", "-vcr", "--delete",) + tuple(
        yield_excludes()
    )
    run(command)

    # post_commands = [
    #     ["find", f"{REMOTE_DIR}/.", "-type", "d", "-exec", "chmod 775", "'{}'", "\\+", "&&"],
    #     ["find", f"{REMOTE_DIR}/.", "-type", "f", "-exec", "chmod 664", "'{}'", "\\+", "&&"],
    #     ["chmod", "+x", f"{REMOTE_DIR}/scripts/*"],
    # ]
    # run(["ssh", remote] + list(chain(*post_commands)), check=True)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--install", action="store_true", help="Flag to install packages on remote"
    )
    parser.add_argument(
        "--config", action="store_true", help="Flag to run configuration on remote"
    )
    parser.add_argument("--period", type=int, help="Time between syncs, default=2s")
    parser.add_argument("--remote", help=f"Remote ip, default:{PI}", default=PI)
    return parser.parse_args()


def main():
    args = parse_args()
    os.chdir(BASEDIR)

    if args.install:
        install(args.remote)

    if args.config:
        config(args.remote)

    while True:
        rsync(remote=args.remote)
        print(f"Waiting {args.period}s for next sync")
        if args.period is None:
            break
        else:
            time.sleep(args.period)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
    except subprocess.SubprocessError:
        print("Failure, please fix!")
