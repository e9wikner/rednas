import os
import sys

import gunicorn.app.wsgiapp

import rednas.avahi


def main():
    os.environ[
        "GUNICORN_CMD_ARGS"
    ] = "--bind 0.0.0.0:21210 --reload --threads 2 --log-level debug"
    sys.argv.append("rednas.api.app:get_app()")
    with rednas.avahi.broadcast_service():
        gunicorn.app.wsgiapp.run()


if __name__ == "__main__":
    main()
