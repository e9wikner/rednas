"""Module to handle (external) disk drives.

This module uses lsblk to monitor what drives are plugged into the device. A 
service can poll this module to get notified when a new drive is connected.

It uses the json output from lsbkl , e.g.:

    $ lsblk -f --json --nodeps
    {
    "blockdevices": [
        {"name":"sda", "fstype":null, "fsver":null, "label":null, "uuid":null, "fsavail":null, "fsuse%":null, "mountpoint":null},
        {"name":"mmcblk0", "fstype":null, "fsver":null, "label":null, "uuid":null, "fsavail":null, "fsuse%":null, "mountpoint":null}
    ]
    }
"""
import json
import logging

import rednas.samba
import rednas.utils
from rednas.exception import RednasException
import rednas

__all__ = ["disks", "BlockDevice", "Partition", "NotMountedError"]
logging.getLogger(__name__).addHandler(logging.NullHandler())


class UnmountTargetBusyError(RednasException):
    pass


class NotMountedError(RednasException):
    pass


class UUIDNotFound(RednasException):
    pass


class NotBtrfsPartition(RednasException):
    pass


class SubvolumeAlreadyExists(RednasException):
    pass


class CouldNotCreateSubvolume(RednasException):
    pass


class BlockDevice:
    @classmethod
    def from_name(cls, name):
        """Initiate a BlockDevice from uuid"""
        all_disks = disks()
        for disk in all_disks:
            if disk.name == name:
                return disk
        raise UUIDNotFound(
            f"Could not find /dev/{name} among all blockdevices:{all_disks}"
        )

    def __init__(self, json):
        """Initiate a BlockDevice from lsblk -f --json output"""
        self.children = []
        for key, value in json.items():
            if key == "children":
                for child_json in value:
                    partition = Partition(child_json, parent=self)
                    self.children.append(partition)
            else:
                self.__setattr__(key, value)

    def __str__(self):
        return json.dumps(self.as_dict())

    def __repr__(self):
        return f"{self.__class__.__name__} {self.__str__()}"

    def as_dict(self):
        return {
            "name": self.name,
            "model": self.model,
            "size": self.size,
            "uuid": self.uuid,
            "children": [c.as_dict() for c in self.children],
        }

    @property
    def is_disk(self) -> bool:
        return self.type == "disk"

    @property
    def has_mounted_children(self):
        return any(c.mounted for c in self.children)

    def wipe(self):
        for child in self.children:
            child.mounted = False
        args = ["disk-init", self.name]
        output = rednas.utils.run_rednas_admin(args)
        logging.info(output)
        logging.info("Disk wiped")


class Partition:
    @classmethod
    def from_uuid(cls, uuid):
        """Create a Partition from uuid"""
        all_disks = disks()
        for disk in all_disks:
            for child in disk.children:
                if child.uuid == uuid:
                    return child
        raise UUIDNotFound(
            f"Could not find UUID {uuid} among all blockdevices:{all_disks}"
        )

    def __init__(self, json, parent):
        """Initiate a Partition from lsblk -f --json output"""
        for key, value in json.items():
            self.__setattr__(key, value)
        self.parent = parent

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"{self.__class__.__name__} {self.__str__()}"

    def as_dict(self):

        return {
            "name": self.name,
            "mounted": self.mounted,
            "mountpoint": self.mountpoint,
            "shared": self.shared,
            "sharepoint": self.sharepoint,
            "filesystem_type": self.fstype,
            "filesystem_size": self.fssize,
            "filesystem_available": self.fsavail,
            "label": self.label,
            "uuid": self.uuid,
            "btrfs_subvolumes": [s.as_dict() for s in self.btrfs_subvolumes],
        }

    @property
    def mounted(self):
        return self.mountpoint is not None

    @mounted.setter
    def mounted(self, mount):
        if self.mounted == mount:
            logging.info(f"{self} is already mounted")
        elif mount:
            self.mountpoint = rednas.mount(self.uuid)
            logging.info(f"{self} mounted @ {self.mountpoint}")
        else:
            rednas.umount(self.mountpoint)
            logging.info(f"{self} mounted @ {self.mountpoint}")
            self.mountpoint = None

    @property
    def sharename(self):
        return f"{self.parent.model}_{self.name}"

    @property
    def shared(self):
        return self.sharename in rednas.samba.get_shares()

    @shared.setter
    def shared(self, share):
        if self.shared == share:
            return

        if share:
            self.mounted = True
            rednas.samba.add_share(self.sharename, self.mountpoint)
        else:
            rednas.samba.remove_share(self.sharepoint)

    @property
    def sharepoint(self):
        if self.shared:
            return self.sharename
        else:
            return None

    @property
    def btrfs_subvolumes(self):
        if self.fstype != "btrfs":
            return []

        unmount_after_check = False
        if not self.mounted:
            self.mounted = True  # To be able to list subvolumes
            unmount_after_check = True
            logging.info(f"Mounting {self} to check for btrfs subvolumes")

        output = rednas.utils.run_rednas_admin(["subvolume-list", self.mountpoint])
        if unmount_after_check:
            self.mounted = False

        # line example: ID 256 gen 11 top level 5 path @Documents
        for line in output.splitlines():
            _, id, _, gen, _, _, parent, _, name = line.split()
            yield SubVolume(name=name, partition=self)


class SubVolume:
    @classmethod
    def from_name_and_partition_uuid(cls, name, uuid):
        return SubVolume(name, Partition.from_uuid(uuid))

    def __init__(self, name, partition):
        self.name = name
        self.parent_partition = partition

    @property
    def mountpoints(self):
        source = f"/dev/disk/by-uuid/{self.parent_partition.uuid}"
        command = [
            "findmnt",
            "--df",
            "--json",
            "-o",
            "+FS-OPTIONS,UUID",
            "--source",
            source,
        ]
        try:
            output = rednas.utils.run(command)

        except rednas.utils.SubprocessError:
            logging.debug(f"Found no mounpoints for {self}")
            return []

        findmnt_json = json.loads(output)
        subvolume_source = f"{self.parent_partition.path}[/{self.name}]"
        return [
            f["target"]
            for f in findmnt_json["filesystems"]
            if f["source"] == subvolume_source
        ]

    def __str__(self):
        return f"{self.name}"

    def __repr__(self):
        return f"{self.__class__.__name__} {self.__str__()}"

    @property
    def mounted(self):
        return len(self.mountpoints) != 0

    @property
    def sharepoints(self):
        mountpoints = self.mountpoints
        shares = rednas.samba.get_shares()
        for share_name, share_dict in shares.items():
            if share_dict["path"] in mountpoints:
                yield share_name

    def as_dict(self):
        return {
            "name": self.name,
            "mountpoints": self.mountpoints,
            "sharepoints": list(self.sharepoints),
            "disk": self.parent_partition.parent.name,
            "users": [],
            # TODO: "users": self.users
        }


def disks():
    """Returns a list of connected BlockDevices that are disks"""
    output = rednas.utils.run("lsblk --json --tree --output-all".split())
    output_json = json.loads(output)
    all_blockdevices = (BlockDevice(oj) for oj in output_json["blockdevices"])
    return [b for b in all_blockdevices if b.is_disk]
