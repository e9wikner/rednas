import logging

import rednas.utils
from rednas.exception import RednasException


logging.getLogger(__name__).addHandler(logging.NullHandler())


class UnmountTargetBusyError(RednasException):
    pass


def mount(uuid, subvolume_name=None):
    """Mount and return mountpoint"""

    command = ["mount", uuid]
    if subvolume_name is not None:
        command += ["--subvol", subvolume_name]

    try:
        mountpoint = rednas.utils.run_rednas_admin(command)
    except rednas.utils.SubprocessError as error:
        logging.error(error)
        raise
    else:
        return mountpoint


def umount(path):
    command = ["umount", str(path)]
    try:
        rednas.utils.run_rednas_admin(command)
    except rednas.utils.SubprocessError as error:
        if "target is busy" in str(error):
            raise UnmountTargetBusyError() from error
        else:
            logging.error(error)
            raise
