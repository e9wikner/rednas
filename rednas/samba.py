"""Python module to create shared folders.

For now all shares are SMB.
"""
import configparser
import getpass
import logging
from pathlib import Path

from rednas.exception import RednasException
from rednas.utils import run_rednas_admin


logging.getLogger(__name__).addHandler(logging.NullHandler())
ORIGINAL_BASE_CONFIG = Path("/etc/samba/smb.conf.original")
BASE_CONFIG = Path("/etc/samba/smb.conf")
REDNAS_CONFIG = BASE_CONFIG.parent / "smb.conf.d" / "rednas.conf"


class SambaException(RednasException):
    pass


def load_samba_config(original=False):
    config = configparser.ConfigParser(interpolation=None)
    if original:
        config.read_string(ORIGINAL_BASE_CONFIG.read_text())
    else:
        config.read_string(BASE_CONFIG.read_text())
    return config


def save_samba_config(config):
    # Write original configuration to file
    if not ORIGINAL_BASE_CONFIG.exists():
        if not BASE_CONFIG.exists():
            raise SambaException(f"Could not find {BASE_CONFIG}, is samba installed?")
        ORIGINAL_BASE_CONFIG.write_text(BASE_CONFIG.read_text())

    logging.info("Writing config to file")

    try:
        with BASE_CONFIG.open(mode="w") as f:
            config.write(f)
    except PermissionError as exc:
        desc = (
            f"I ({getpass.getuser()}) have no permission to write a new"
            f"samba config ({BASE_CONFIG})"
        )
        raise SambaException(desc) from exc


def restart_server():
    run_rednas_admin(["samba-restart"])


def get_shares():
    """List of all available SHAREs on this system."""
    config = load_samba_config()
    shares = [
        s
        for s in config.sections()
        if s not in {"global", "homes", "printers", "print$"}
    ]
    return {s: dict(config[s]) for s in shares}


def set_shares(shares):
    """Create a shared folder"""
    logging.info(f"Setting shares to: {shares}")
    config = load_samba_config(original=True)
    for share in shares:
        config[share] = {
            "comment": share,
            "path": shares[share]["path"],
            "valid users": "e9wikner, @sambashare",
            "writable": "yes",
            "guest ok": "yes",
        }
    save_samba_config(config)
    restart_server()


def add_share(name, path, users=None):
    logging.info(f"Sharing {path}->{name} for users: {users}")
    shares = get_shares()

    new_share = {"name": name, "path": path}
    if users is not None:
        new_share["users"] = ", ".join(users)

    shares[name] = new_share
    set_shares(shares)


def remove_share(name):
    shares = get_shares()
    del shares[name]
    set_shares(shares)
