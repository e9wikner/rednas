import json
from dataclasses import dataclass, asdict
from pathlib import Path
import logging

import rednas.utils
from rednas.exception import RednasException
from rednas.blockdevice import BlockDevice

logging.getLogger(__name__).addHandler(logging.NullHandler())


class BtrfsException(RednasException):
    pass


class BtrfsRootNotFound(BtrfsException):
    pass


class SubvolumeAlreadyExists(BtrfsException):
    pass


class BadSubvolumeName(BtrfsException):
    pass


class CouldNotCreateSubvolume(BtrfsException):
    pass


class CouldNotDeleteSubvolume(BtrfsException):
    pass


class Btrfs:
    """Represents a BTRFS fileystem.

    Important: It's the responsibility of the user to call refresh() on this object
    when latest data is needed. Since e.g. bytes_used can change quite often it would
    take too much time to do this in the background.
    """

    @classmethod
    def yield_all(cls):
        filesystem_show_output = cls.show()
        for line in filesystem_show_output.splitlines():
            if "Label: " in line and "uuid: " in line:
                yield Btrfs(line.split()[-1])

    @staticmethod
    def show(uuid=None):
        args = []
        if uuid is not None:
            args.append("--uuid")
            args.append(uuid)

        return rednas.utils.run_rednas_admin(["filesystem-show"] + args)

    @staticmethod
    def findmnt_filesystems():
        command = ["findmnt", "--list", "--json", "-o", "+FS-OPTIONS,UUID"]
        output = rednas.utils.run(command)
        findmnt_json = json.loads(output)
        return findmnt_json["filesystems"]

    def __init__(self, uuid):
        """Instantiates a Btrfs object and calls refresh() on it"""
        self.uuid = uuid
        self.refresh()

    def __str__(self):
        return f"label: {self.label}, uuid: {self.uuid}"

    def __repr__(self):
        return f"{self.__class__.__name__} {self.__str__()}"

    def as_dict(self):
        return {
            "devices": [asdict(d) for d in self.devices.values()],
            "bytes_used": self.bytes_used,
            "label": self.label,
            "mountpoint": self.mountpoint,
            "total_devices": self.total_devices,
            "uuid": self.uuid,
        }

    def refresh(self):
        """Refresh filesystem and all properties."""

        # Label: 'label_disk_raid0'  uuid: 8b46c91e-1aa7-4fdc-a513-c44cf0ecf89e
        # 	Total devices 2 FS bytes used 2004221120512
        # 	devid    1 size 4000785960960 used 1005030735872 path /dev/sdc1
        # 	devid    2 size 4000785960960 used 1005030735872 path /dev/sdd1

        devices = dict()
        for line_raw in self.show(self.uuid).splitlines():
            line = line_raw.strip()
            line_split = line.split()
            if line.startswith("Label: "):
                label_raw = line.split()[1]
            elif line.startswith("Total devices "):
                total_devices_raw = line_split[2]
                bytes_used_raw = line_split[-1]
            elif line.startswith("devid"):
                _, dev_id_raw, _, size_raw, _, used_raw, _, path_raw = line.split()
                size = int(size_raw)
                used = int(used_raw)
                dev_id = int(dev_id_raw)
                devices[dev_id] = BtrfsDevice(size=size, used=used, path=path_raw)

        if label_raw == "none":
            self._label = None
        else:
            self._label = label_raw.strip("'")
        self._total_devices = int(total_devices_raw)
        self._bytes_used = int(bytes_used_raw)
        self._devices = devices

    @property
    def bytes_used(self):
        return self._bytes_used

    @property
    def label(self):
        return self._label

    @property
    def total_devices(self):
        return self._total_devices

    @property
    def devices(self):
        self.refresh()
        return self._devices

    @property
    def mounted(self):
        return self.mountpoint is not None

    @mounted.setter
    def mounted(self, mount):
        if self.mounted == mount:
            logging.info(f"{self} is already mounted")
        elif mount:
            self.mountpoint = rednas.command.mount(self.uuid)
            logging.info(f"{self} mounted @ {self.mountpoint}")
        else:
            rednas.command.umount(self.mountpoint)
            logging.info(f"{self} mounted @ {self.mountpoint}")
            self.mountpoint = None

    @property
    def mountpoint(self):
        findmnt = self.findmnt_filesystems()
        for filesystem in findmnt:
            if filesystem["uuid"] == self.uuid:
                if "[" not in filesystem["source"]:
                    return filesystem["target"]


@dataclass
class BtrfsDevice:
    size: int
    used: int
    path: str


#
# class SubVolume:
#     @classmethod
#     def from_name_and_partition_uuid(cls, name, uuid):
#         return SubVolume(name, .from_uuid(uuid))
#
#     def __init__(self, name, partition):
#         self.name = name
#         self.parent_partition = partition
#
#     @property
#     def mountpoints(self):
#         source = f"/dev/disk/by-uuid/{self.parent_partition.uuid}"
#         command = [
#             "findmnt",
#             "--df",
#             "--json",
#             "-o",
#             "+FS-OPTIONS,UUID",
#             "--source",
#             source,
#         ]
#         try:
#             output = rednas.utils.run(command)
#
#         except rednas.utils.SubprocessError:
#             logging.debug(f"Found no mounpoints for {self}")
#             return []
#
#         findmnt_json = json.loads(output)
#         subvolume_source = f"{self.parent_partition.path}[/{self.name}]"
#         return [
#             f["target"]
#             for f in findmnt_json["filesystems"]
#             if f["source"] == subvolume_source
#         ]
#
#     def __str__(self):
#         return f"{self.name}"
#
#     def __repr__(self):
#         return f"{self.__class__.__name__} {self.__str__()}"
#
#     @property
#     def mounted(self):
#         return len(self.mountpoints) != 0
#
#     @property
#     def sharepoints(self):
#         mountpoints = self.mountpoints
#         shares = rednas.samba.get_shares()
#         for share_name, share_dict in shares.items():
#             if share_dict["path"] in mountpoints:
#                 yield share_name
#
#     def as_dict(self):
#         return {
#             "name": self.name,
#             "mountpoints": self.mountpoints,
#             "sharepoints": list(self.sharepoints),
#             "disk": self.parent_partition.parent.name,
#             "users": [],
#             # TODO: "users": self.users
#         }
# def find_btrfs_root_volume_partition(disk_name):
#     """Search for the Btrfs volume root partition in DISK_NAME
#
#     :param disk_name: Name of disk, e.g. sda
#     :raises rednas.blockdevice.UUIDNotFound: when DISK_NAME does not exist on
#     the server
#     :raises BtrfsRootNotFound: When DISK_NAME does not correspond to a disk that
#     contains one partition that is also a BTRFS top-level subvolume
#     """
#     disk = BlockDevice.from_name(disk_name)  # raises UUIDNotFound
#     if len(disk.children) != 1:
#         raise BtrfsRootNotFound(
#             f"{disk_name} has more than one partitions: {', '.join(disk.children)}"
#         )
#     partition = disk.children[0]
#     if partition.fstype != "btrfs":
#         raise BtrfsRootNotFound(f"{partition.name} is not a BTRFS top-level subvolume")
#     return partition
#
#
# def add_subvolume(name: str, disk_name: str, users: list) -> None:
#     """Add a BTRFS subvolume in the BTRFS root of DISK_NAME
#
#     :param name: Name of the subvolume
#     :param disk_name: Name of the disk where the subvolume is created
#     :param users: List of users that should have access to the subvolume
#     :raises rednas.blockdevice.UUIDNotFound: when DISK_NAME does not exist on
#     the server
#     :raises BtrfsRootNotFound: When DISK_NAME does not correspond to a disk that
#     contains one partition that is also a BTRFS top-level subvolume
#     :raises UserNotFound: When USERS contains a username that does not exist
#     on the server
#     :raises SubvolumeAlreadyExists: When NAME already exists in DISK_NAME top-level
#     subvolume
#     :raises BadSubvolumeName: When NAME is not a proper filename or subvolume name
#     :raises CouldNotCreateSubvolume: When the 'btrfs subvolume' command failed
#     """
#     btrfs_root_partition = find_btrfs_root_volume_partition(disk_name)
#
#     if name in btrfs_root_partition.btrfs_subvolumes:
#         raise SubvolumeAlreadyExists(f"{name} already exists in {btrfs_root_partition}")
#
#     btrfs_root_partition.mounted = True
#     root = Path(btrfs_root_partition.mountpoint)
#     destination = root / name
#     if not root.samefile(destination.parent):
#         raise BadSubvolumeName(f"{name} is not a proper filename or subvolume name")
#
#     command = ["subvolume-create", str(destination)]
#     try:
#         rednas.utils.run_rednas_admin(command)
#     except rednas.utils.SubprocessError as error:
#         raise CouldNotCreateSubvolume from error
#
#
# def delete_subvolume(name: str, disk_name: str) -> None:
#     """Delete btrfs subvolume NAME in DISK_NAME btrfs volume root
#
#     :raises rednas.blockdevice.UUIDNotFound: when DISK_NAME does not exist on
#     the server
#     :raises BtrfsRootNotFound: When DISK_NAME does not correspond to a disk that
#     contains one partition that is also a BTRFS top-level subvolume
#     """
#     btrfs_root_partition = find_btrfs_root_volume_partition(disk_name)
#     btrfs_root_partition.mounted = True
#     root_mountpoint = btrfs_root_partition.mountpoint
#     subvolume_path = Path(root_mountpoint) / name
#     logging.info(f"Deleting subvolume at {subvolume_path}")
#     command = ["subvolume-delete", str(subvolume_path.absolute())]
#     try:
#         rednas.utils.run_rednas_admin(command)
#     except rednas.utils.SubprocessError as error:
#         raise CouldNotDeleteSubvolume from error
