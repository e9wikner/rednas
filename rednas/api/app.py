import logging
import os

import falcon

from rednas.api import btrfs, files, hello, index, redsync, samba, storage, volumes

REDSYNC_ROOT = os.environ.get("REDSYNC_ROOT", default="/srv/rednas/photos")


class MiddleWare:
    def process_response(self, req, resp, resource, req_succeeded):
        """Post-processing of the response (after routing).

        Args:
            req: Request object.
            resp: Response object.
            resource: Resource object to which the request was
                routed. May be None if no route was found
                for the request.
            req_succeeded: True if no exceptions were raised while
                the framework processed and routed the request;
                otherwise False.
        """
        resp.append_header("Access-Control-Allow-Origin", "*")


def create_app():
    api = falcon.API(middleware=[MiddleWare()])
    base_url = "/rednas/api/v0.1"

    url_to_handler = {
        "/hello/{device_id}": hello.Hello(REDSYNC_ROOT),
        "/index/{device_id}": index.Index(REDSYNC_ROOT),
        "/files/{device_id}/{identifier}": files.File(REDSYNC_ROOT),
        "/disks": storage.Devices(base_url),
        "/disks/{name}/format": storage.FormatDisk(base_url),
        "/partition/{uuid}": storage.Partition(base_url),
        "/partition/{uuid}/subvolume/{name}": storage.PartitionSubvolume(base_url),
        "/redsync": redsync.RedSync(),
        "/samba/shares": samba.Shares(),
        "/samba/shares/{name}": samba.Share(),
        "/volumes": volumes.Volumes(),
        "/volumes/{disk_name}/{name}": volumes.Volume(),
        "/btrfs": btrfs.Btrfs(),
    }
    for url, handler in url_to_handler.items():
        api.add_route(base_url + url, handler)

    return api


def get_app():
    logging.basicConfig(
        level=logging.INFO,
        format="[%(asctime)s] [%(process)s] [%(levelname)s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S %z",
    )
    return create_app()
