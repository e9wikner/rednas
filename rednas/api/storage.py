import json
import logging

import falcon

import rednas.blockdevice


logging.getLogger(__name__).addHandler(logging.NullHandler())


class Devices:
    @classmethod
    def get_devices(cls, req, resp, base_url):
        available_blockdevices = rednas.blockdevice.disks()
        if len(available_blockdevices) == 0:
            resp.code = falcon.HTTP_NOT_FOUND
        else:
            blockdevices_as_dicts = [b.as_dict() for b in available_blockdevices]
            for b in blockdevices_as_dicts:
                b["relative_uri"] = f"{base_url}/disks/{b['name']}"

                b["children"] = [c for c in b["children"] if c["uuid"] is not None]
                for child in b["children"]:
                    partition_relative_uri = f"{base_url}/partition/{child['uuid']}"
                    child["relative_uri"] = partition_relative_uri

                    for subvolume in child["btrfs_subvolumes"]:
                        subvolume[
                            "relative_uri"
                        ] = f"{partition_relative_uri}/subvolume/{subvolume['name']}"

            body = {"blockdevices": blockdevices_as_dicts}
            resp.body = json.dumps(body)
        logging.info(f"resp.code: {resp.status}")

    def __init__(self, base_url):
        self.base_url = base_url

    def on_get(self, req, resp):
        return self.get_devices(req, resp, self.base_url)


class FormatDisk:
    def __init__(self, base_url):
        self.base_url = base_url

    def on_get(self, req, resp, name):
        logging.info(f"Wiping disk {name}")
        disk = rednas.blockdevice.BlockDevice.from_name(name)
        disk.wipe()
        Devices.get_devices(req, resp, self.base_url)


class Partition:
    def __init__(self, base_url):
        self.base_url = base_url

    def on_get(self, req, resp, uuid):
        partition = rednas.blockdevice.Partition.from_uuid(uuid)
        logging.info(f"Get {partition} with params:{req.params}")

        try:
            if "mount" in req.params:
                partition.mounted = req.params["mount"].lower() == "true"

            if "share" in req.params:
                partition.shared = req.params["share"].lower() == "true"
        except rednas.blockdevice.UnmountTargetBusyError:
            logging.error("Target it busy")
            resp.status = falcon.HTTP_INTERNAL_SERVER_ERROR

        # TODO: Handle more errors here, but for now it's good with the stacktrace

        Devices.get_devices(req, resp, self.base_url)


class PartitionSubvolume:
    def __init__(self, base_url):
        self.base_url = base_url

    def on_get(self, req, resp, uuid, name):
        subvolume = rednas.blockdevice.SubVolume.from_name_and_partition_uuid(
            name, uuid
        )

        # TODO: make function of this since it's identical for Partition
        try:
            if "mount" in req.params:
                subvolume.mounted = req.params["mount"].lower() == "true"

            if "share" in req.params:
                subvolume.shared = req.params["share"].lower() == "true"

        except rednas.blockdevice.UnmountTargetBusyError:
            logging.error("Target it busy")
            resp.status = falcon.HTTP_INTERNAL_SERVER_ERROR

        Devices.get_devices(req, resp, self.base_url)
