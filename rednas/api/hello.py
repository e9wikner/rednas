import json
import logging

import falcon

from rednas.filedb import FileDB

logging.getLogger(__name__).addHandler(logging.NullHandler())


class Hello:
    def __init__(self, rednas_root):
        self.rednas_root = rednas_root

    def on_get(self, req, resp, device_id):
        logging.info(
            f"received Hello from {device_id}, initiating FileDB in {self.rednas_root}"
        )
        try:
            db = FileDB(root=self.rednas_root, device_id=device_id)
            db.create_files_table()

            logging.info(f"created local_v0_db {db}")
            resp.body = json.dumps({"device_id": db.device_id})

        except Exception as exc:
            logging.error(exc)
            raise falcon.HTTPInternalServerError(description=str(exc))
