import json
import logging

import rednas.btrfs

logging.getLogger(__name__).addHandler(logging.NullHandler())


class Btrfs:
    def on_get(self, req, resp):
        btrfs = rednas.btrfs.Btrfs.yield_all()
        as_dicts = [b.as_dict() for b in btrfs]
        resp.body = json.dumps({"btrfs": as_dicts})
