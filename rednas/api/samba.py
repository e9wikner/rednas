import json
import logging

import falcon

import rednas.samba


logging.getLogger(__name__).addHandler(logging.NullHandler())


def translate_samba_users(share_config_dict):
    users = []
    if "valid users" in share_config_dict:
        users = share_config_dict["valid users"].split(", ")
    else:
        users = []

    if "guest ok" in share_config_dict and share_config_dict["guest ok"] == "yes":
        users.append("guest")

    return users


class Shares:
    def on_get(self, req, resp):
        try:
            shares = rednas.samba.get_shares()
        except Exception as exc:
            # TODO: handle defined exceptions
            logging.error(exc)
            shares = dict()

        shares_as_list = []
        for share_name, share_config_dict in shares.items():
            share_config_dict["name"] = share_name
            share_config_dict["users"] = translate_samba_users(share_config_dict)

            shares_as_list.append(share_config_dict)

        resp.body = json.dumps({"shares": shares_as_list})
        logging.info(f"Sending: {resp.body}")

    def on_post(self, req, resp):
        req_json = json.load(req.bounded_stream)
        logging.info(f"Post {req_json}")
        share = req_json["share"]
        try:
            rednas.samba.add_share(**share)
        except rednas.samba.SambaException as err:
            logging.error(err)
            raise falcon.HTTPInternalServerError(description=str(err))


class Share:
    def on_delete(self, req, resp, name):
        logging.info(f"Deleting share {name}")
        try:
            rednas.samba.remove_share(name)
        except rednas.samba.SambaException as err:
            logging.error(err)
            raise falcon.HTTPInternalServerError(description=str(err))
