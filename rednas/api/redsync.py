import json
import logging

import falcon

import rednas.btrfs

logging.getLogger(__name__).addHandler(logging.NullHandler())


class RedSync:
    def on_get(self, req, resp):
        try:

            btrfs = rednas.btrfs.Btrfs.yield_all()
            roots = [b.label if b.label else b.uuid for b in btrfs]
            resp.body = json.dumps({"roots": roots})

        except Exception as exc:
            logging.error(exc)
            raise falcon.HTTPInternalServerError(description=str(exc))
