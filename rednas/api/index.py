import json
import logging

import rednas.filedb

logging.getLogger(__name__).addHandler(logging.NullHandler())


class Index:
    def __init__(self, redsync_root):
        self.redsync_root = redsync_root

    def on_put(self, request, response, device_id):
        logging.info(f"Receiving new index from {device_id}")

        db = rednas.filedb.FileDB(root=self.redsync_root, device_id=device_id)

        db.sync_remote_index(file_like_object=request.bounded_stream)

        response.body = json.dumps(
            {
                "missing": sorted(db.identifiers_to_be_updated),
                "remove": sorted(db.identifiers_to_be_removed),
            }
        )
