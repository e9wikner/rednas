import itertools
import json
import logging

import falcon

import rednas.blockdevice
import rednas.btrfs

logging.getLogger(__name__).addHandler(logging.NullHandler())


class Volumes:
    def on_get(self, req, resp):
        disks = rednas.blockdevice.disks()
        partitions = itertools.chain.from_iterable(d.children for d in disks)
        subvolumes = itertools.chain.from_iterable(
            p.btrfs_subvolumes for p in partitions
        )
        subvolumes_as_dicts = [s.as_dict() for s in subvolumes]
        resp.body = json.dumps({"volumes": subvolumes_as_dicts})

    def on_post(self, req, resp):
        req_json = json.load(req.bounded_stream)
        logging.info(f"Post {req_json}")
        volume = req_json["volume"]
        try:
            rednas.btrfs.add_subvolume(**volume)
        except rednas.btrfs.BtrfsException as err:
            logging.error(err)
            raise falcon.HTTPInternalServerError(description=str(err))


class Volume:
    def on_delete(self, req, resp, disk_name, name):
        try:
            rednas.btrfs.delete_subvolume(name, disk_name)
        except rednas.samba.SambaException as err:
            logging.error(err)
            raise falcon.HTTPInternalServerError(description=str(err))
